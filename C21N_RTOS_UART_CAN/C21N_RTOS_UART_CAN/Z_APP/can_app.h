/*
 * can_app.h
 *
 * Created: 1/10/2019 2:01:11 AM
 *  Author: lmphi
 */ 


#ifndef CAN_APP_H_
#define CAN_APP_H_



#include "app_include.h"


#define TASK_CAN_RX_STACK_SIZE (64 / sizeof(portSTACK_TYPE))
#define TASK_CAN_RX_STACK_PRIORITY (tskIDLE_PRIORITY + 0)

#define TASK_CAN_TX_STACK_SIZE (64 / sizeof(portSTACK_TYPE))
#define TASK_CAN_TX_STACK_PRIORITY (tskIDLE_PRIORITY + 0)

//TaskHandle_t Can_RX_tHandle;
//TaskHandle_t Can_TX_tHandle;
QueueHandle_t Can_RX_Queue;
QueueHandle_t Can_TX_Queue;

extern void can_init (void);





#endif /* CAN_APP_H_ */