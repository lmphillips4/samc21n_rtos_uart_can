/*
 * can_app.c
 *
 * Created: 1/10/2019 2:01:24 AM
 *  Author: lmphi
*/ 


#include "can_app.h"


static void can_rx_task (void *p);
static void can_tx_task (void *p);


void CAN0_tx_cb(struct can_async_descriptor *const descr)
{
	//(void)descr;
}

void CAN0_rx_cb(struct can_async_descriptor *const descr)
{
	//struct can_message msg;
	//uint8_t            data[64];
	//msg.data = data;
	//can_async_read(descr, &msg);
	//
	//xQueueSendFromISR(Can_RX_Queue, &msg, NULL);
	//return;
}


void can_init (void)
{	
	// enable
	can_async_register_callback(&CAN_0, CAN_ASYNC_TX_CB, (FUNC_PTR)CAN0_tx_cb);
	can_async_enable(&CAN_0);
	
	// create queues
	Can_RX_Queue = xQueueCreate(2, sizeof(struct can_message *));
	Can_TX_Queue = xQueueCreate(2, sizeof(struct can_message *));
	
	if (Can_RX_Queue == NULL || Can_TX_Queue == NULL) {
		while (1) {  // handle error here
			delay_ms(200);
			gpio_toggle_pin_level(LED0);
		}
	}
	
	// create rx task
	if (xTaskCreate(can_rx_task, "can_rx", TASK_CAN_RX_STACK_SIZE, NULL, TASK_CAN_RX_STACK_PRIORITY, NULL) != pdPASS) {
		while (1) {  // handle error here
			delay_ms(200);
			gpio_toggle_pin_level(LED0);
		}
	}
	
	// create tx task
	if (xTaskCreate(can_tx_task, "can_tx", TASK_CAN_TX_STACK_SIZE, NULL, TASK_CAN_TX_STACK_PRIORITY, NULL) != pdPASS) {
		while (1) {  // handle error here
			delay_ms(200);
			gpio_toggle_pin_level(LED0);
		}
	}
	
}

void can_rx_task (void *p)
{
	(void)p;
	struct can_message msgRx;
	struct can_filter  rxFilter0;
	struct can_filter  rxFilter1;
	static uint8_t myStr[100];
	
	//static uint8_t myStr[512];
	
	can_async_register_callback(&CAN_0, CAN_ASYNC_RX_CB, (FUNC_PTR)CAN0_rx_cb);
	
	rxFilter0.id   = 0x100;
	rxFilter0.mask = 0;
	can_async_set_filter(&CAN_0, 0, CAN_FMT_STDID, &rxFilter0);
		
	rxFilter1.id   = 0x101;
	rxFilter1.mask = 0;
	can_async_set_filter(&CAN_0, 1, CAN_FMT_STDID, &rxFilter1);
	
	for (;;) {
		xQueueReceive(Can_RX_Queue, &msgRx, portMAX_DELAY);
		
		sprintf(myStr, "<CAN_RX> :\r\n"
		"\t\t id : %#X\r\n"
		"\t\t data :");
		str_write(myStr);
		
		for (uint32_t i=0; i < msgRx.len; i++) {
			sprintf(myStr, " %u", msgRx.data[i]);
			str_write(myStr);
		}
		str_write("\r\n");
		os_sleep(10000);
	}
}


void can_tx_task (void *p)
{
	(void)p;
	struct can_message msgTx;
	uint8_t            send_data[4];
	send_data[0] = 0x00;
	send_data[1] = 0x01;
	send_data[2] = 0x02;
	send_data[3] = 0x09;
	static uint8_t myStr[100];

	msgTx.id   = 0x45A;
	msgTx.type = CAN_TYPE_DATA;
	msgTx.data = send_data;
	msgTx.len  = 4;
	msgTx.fmt  = CAN_FMT_STDID;
	
	//static uint8_t myStr[512];
	
	can_async_register_callback(&CAN_0, CAN_ASYNC_TX_CB, (FUNC_PTR)CAN0_tx_cb);
	
	for (;;) {
		xQueueReceive(Can_TX_Queue, &msgTx, portMAX_DELAY);
		
		can_async_write(&CAN_0, &msgTx);
		
		sprintf(myStr, "<CAN_TX> :\r\n"
		"\t\t id : %#X\r\n"
		"\t\t data :");
		str_write(myStr);
		
		for (uint32_t i=0; i < msgTx.len; i++) {
			sprintf(myStr, " %u", msgTx.data[i]);
			str_write(myStr);
		}
		str_write("\r\n");
		os_sleep(10000);
		
	}
	
}



