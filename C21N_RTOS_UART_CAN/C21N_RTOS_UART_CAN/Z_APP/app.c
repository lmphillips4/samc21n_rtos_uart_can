/*
 * app.c
 *
 * Created: 1/10/2019 12:31:42 AM
 *  Author: lmphi
*/ 


#include "app.h"



static void _os_show_statistics(void);




void app_init (void)
{
	disp_mutex = xSemaphoreCreateMutex();

	if (disp_mutex == NULL) {
		while (1) {  // handle error here
			delay_ms(100);
			gpio_toggle_pin_level(LED0);
		}
	}
	
	if (xTaskCreate(task_led, "Led", TASK_LED_STACK_SIZE, NULL, TASK_LED_STACK_PRIORITY, &xCreatedLedTask) != pdPASS) {
		while (1) {  // handle error here
			delay_ms(100);
			gpio_toggle_pin_level(LED0);
		}
	}
	
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL, TASK_MONITOR_STACK_PRIORITY, &xCreatedMonitorTask) != pdPASS) {
		while (1) {  // handle error here
			delay_ms(100);
			gpio_toggle_pin_level(LED0);
		}
	}
}


void task_led(void *p)
{
	(void)p;
	
	for (;;) {
		gpio_toggle_pin_level(LED0);
		os_sleep(500);
	}
}


void task_monitor(void *p)
{
	(void)p;
	
	for (;;) {
		//if (xSemaphoreTake(disp_mutex, ~0)) {
		str_write("hi\r\n");
			//_os_show_statistics();
			//xSemaphoreGive(disp_mutex);
			//}

		os_sleep(2000);
	}
}


void _os_show_statistics(void)
{
	portCHAR szList[256];
	
	//str_write("hi\r\n");
	sprintf(szList, "\n\n\n%c%c%c%c", 0x1B, '[', '2', 'J');
	str_write(szList);
	sprintf(szList, "--- Number of tasks: %u\r\n", (unsigned int)uxTaskGetNumberOfTasks());
	str_write(szList);
	str_write("> Tasks\tState\tPri\tStack\tNum\r\n");
	str_write("***********************************\r\n");
	vTaskList(szList);
	str_write(szList);
}