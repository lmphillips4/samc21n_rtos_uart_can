/*
 * app.h
 *
 * Created: 1/10/2019 12:31:50 AM
 *  Author: lmphi
 */ 


#ifndef APP_H_
#define APP_H_



#include "app_include.h"


#define TASK_LED_STACK_SIZE (64 / sizeof(portSTACK_TYPE) / 1)
#define TASK_LED_STACK_PRIORITY (tskIDLE_PRIORITY + 1)

#define TASK_MONITOR_STACK_SIZE (1024 / sizeof(portSTACK_TYPE) / 1)
#define TASK_MONITOR_STACK_PRIORITY (tskIDLE_PRIORITY + 2)

static SemaphoreHandle_t disp_mutex;
static TaskHandle_t      xCreatedLedTask;
static TaskHandle_t      xCreatedConsoleTask;
static TaskHandle_t      xCreatedMonitorTask;





extern void app_init(void);
extern void task_led(void *p);
extern void task_monitor(void *p);







#endif /* APP_H_ */