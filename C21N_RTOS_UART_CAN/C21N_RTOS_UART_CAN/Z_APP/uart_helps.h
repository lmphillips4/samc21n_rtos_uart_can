/*
 * uart_helps.h
 *
 * Created: 1/10/2019 12:32:01 AM
 *  Author: lmphi
 */ 


#ifndef UART_HELPS_H_
#define UART_HELPS_H_



#include "app_include.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>





extern void str_write(const char *s);
extern char char_read(void);


#endif /* UART_HELPS_H_ */